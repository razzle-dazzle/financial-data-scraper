const parser = require('node-html-parser').parse;
const axios = require('axios');
const xml2js = require('xml2js');
const xmlParser = xml2js.parseString;
const args = require('yargs').argv;

let parserOptions = {
  // lowerCaseTagName: false,  // convert tag name to lower case (hurt performance heavily)
  script: false, // retrieve content in <script> (hurt performance slightly)
  style: false, // retrieve content in <style> (hurt performance slightly)
  pre: false, // retrieve content in <pre> (hurt performance slightly)
  comment: false, // retrieve comments (hurt performance slightly)
};

let searchTypes = [
  'eps-earnings-per-share-diluted',
  'revenue',
  'operating-income',
  'ebitda',
  'gross-profit',
  'net-income',
  'ebitda',
  'shares-outstanding',
];

// 'GOOG'
if(args.code)
  run(args.code.toUpperCase(), searchTypes).then((dataTables)=>{
    // dataTables is a list of lists of tables
    dataTables.forEach((table, index) => {
      console.log(`TABLE ${index}`);
      let display = '';
      table.head.forEach((cell, index) => {
        if (index != 0) display += '  ';
        display += cell;
      });
      console.log(display);
      table.rows.forEach((row) => {
        let display = '';
        row.forEach((cell, index) => {
          if (index != 0) display += '  ';
          display += cell;
        });
        console.log(display);
      });
    });
  })
  else
  console.log("Please provide 4 letter company code e.g.\nnode index.js --code=GOOG"
  );


async function run(code, searchTypes) {
  return new Promise(async (resolve,reject)=>{
    const url = await urlFromCode(code);
    const promises = [];
    
    searchTypes.map((searchType) => {
      promises.push(getDataFromTables(`${url}${searchType}`));
    });
    
    Promise.all(promises)
    .then((res) =>{ 
      console.log('RESOLVED TRABLES', res)
      // Flattens list of promises which return lists of tables
      const tables = res.flat();

      resolve(tables)
    })
    .catch((error) => {
      console.log(`Error in executing ${error}`)
      reject('failed')
    });
  })
  // const tables = getDataFromTables(`${url}${searchType}`);
}

async function urlFromCode(code) {
  return new Promise(async (resolve, reject) => {
    await axios
      .get(`https://www.macrotrends.net/stocks/charts/${code}`)
      .then((res) => console.log(res))
      .catch((res) => {
        // When going to url/{code}, the site redirects to the correct path
        // I'm then picking out that correct path so I can use it for the 
        // actual requests
        // Requesting this: https://www.macrotrends.net/stocks/charts/SEDG/
        // Gives me /stocks/charts/SEDG/solaredge-technologies/
        console.log(res.request.path);
        resolve(`https://www.macrotrends.net${res.request.path}`);
      });
  });
}

async function getDataFromTables(url) {
  return new Promise(async (resolve, reject) => {
    // let response = await axios.get("https://www.macrotrends.net/stocks/charts/SEDG/solaredge-technologies/eps-earnings-per-share-diluted")
    // let response = await axios.get("https://www.macrotrends.net/stocks/charts/FSLR/first-solar/eps-earnings-per-share-diluted")
    // let response = await axios.get("https://www.macrotrends.net/stocks/charts/SEDG/solaredge-technologies/net-income")
    let response = await axios.get(url);

    // let xml = xmlParser(response.data)
    const stringData = response.data.replace(/\n|\r|\t/g, '');
    const html = parser(stringData, parserOptions);
    let tables = html.querySelectorAll('table');

    let dataTables = [];

    tables.forEach((table, index) => {
      let thead = getTableHead(table);
      let headCells = [];

      if (thead && thead.text.search(/previewhtml/i) == -1) {
        headCells = getTableHeadCells(thead);
      } else return;

      let rows = getTableBodyRows(table);

      dataTables.push({
        head: headCells,
        rows,
      });
    });
    resolve(dataTables);
  });
}

// run()

function getTableHead(table) {
  let tableHead = table.querySelector('thead');
  return tableHead;
}
function getTableHeadCells(thead = []) {
  let result = [];
  let thCells = thead.querySelectorAll('th');
  thCells.forEach((cell) => {
    let cellText = cell.text;
    result.push(cellText);
  });
  return result;
}

function getTableBodyRows(table) {
  let tableBody = table.querySelector('tbody');
  let rows = tableBody.querySelectorAll('tr');
  let result = [];
  rows.forEach((row) => {
    let cells = getRowTdCells(row);
    result.push(cells);
  });
  return result;
}

function getRowTdCells(row) {
  let cells = row.querySelectorAll('td');
  let result = [];
  cells.forEach((cell) => {
    result.push(cell.text);
  });
  return result;
}
